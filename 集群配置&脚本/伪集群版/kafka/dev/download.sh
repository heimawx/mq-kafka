#!/bin/sh -e

FILENAME="kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz"
SCALA_FILENAME="scala-${SCALA_DOWNLOAD_VERSION}.tgz"


url=https://mirrors.cloud.tencent.com/apache/kafka/${KAFKA_VERSION}/${FILENAME}
scala_url=https://downloads.lightbend.com/scala/${SCALA_DOWNLOAD_VERSION}/${SCALA_FILENAME}

echo "Downloading Kafka from $url"

axel -an 10  -o /tmp/${FILENAME}  ${url}

echo "Downloading scala from $scala_url"

axel -an 10  -o /tmp/${SCALA_FILENAME}  ${scala_url}
